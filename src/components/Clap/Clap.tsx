import React, { FunctionComponent } from 'react';
import './Clap.css';
import { ReactComponent as ClapIcon } from './clap-icon.svg';

export interface IClapProps {
  claps: number;
  clapped: () => void;
}
const Clap: FunctionComponent<IClapProps> = props => {
  let clapInstance: SVGSVGElement;
  const doClap = () => {
    animateClap(clapInstance, 'bounceIn');
    props.clapped();
  };
  return (
    <div className="clap">
      <ClapIcon
        ref={c => {
          if (c) {
            clapInstance = c;
          }
        }}
        onClick={doClap}
      />
      <span className="count">{props.claps}</span>
    </div>
  );
};

export default Clap;

function animateClap(
  element: SVGSVGElement,
  animationName: string,
  callback?: () => void
) {
  element.classList.add(animationName);

  function handleAnimationEnd() {
    element.classList.remove(animationName);
    element.removeEventListener('animationend', handleAnimationEnd);

    if (typeof callback === 'function') callback();
  }

  element.addEventListener('animationend', handleAnimationEnd);
}
