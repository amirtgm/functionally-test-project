import React, { FunctionComponent } from 'react'
import { ITag } from '../../types'
import './Tag.css'

const Tag: FunctionComponent<ITag> = props => {
  return <span className="tag">{props.name}</span>
}

export default Tag
