import React, { Component } from 'react'
import logo from './logo.svg'
import Article from '../Article/Article'
import './App.css'

class App extends Component {
  public render() {
    return (
      <div className="App">
        <Article />
      </div>
    )
  }
}

export default App
