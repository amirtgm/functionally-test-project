import React, { FunctionComponent } from 'react';
import './Comments.css';

interface ICommentProps {
  name: string;
  content: string;
  avatar: string;
}
const Tag: FunctionComponent<ICommentProps> = props => {
  return (
    <div className="comment">
      <div className="comment-header">
        <img
          src={`${process.env.PUBLIC_URL}/avatar/${props.avatar}.svg`}
          alt={props.name}
        />
        <span>{props.name}</span>
      </div>
      <div className="comment-body">
        <p
          className="serif"
          dangerouslySetInnerHTML={{ __html: props.content }}
        />
      </div>
    </div>
  );
};

export default Tag;
