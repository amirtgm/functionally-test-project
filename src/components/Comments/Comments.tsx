import React, { Component, createRef } from 'react';
// import logo from './logo.svg'
import { IComment } from '../../types';
import Comment from './Comment';
import { ReactComponent as CommentIcon } from './comment.svg';
import './Comments.css';
interface ICommentState {
  writing: boolean;
  comment: string;
}
interface ICommentProps {
  comments: IComment[];
  addComment: (arg0: string) => void;
}
class Comments extends Component<ICommentProps, ICommentState> {
  constructor(props: ICommentProps) {
    super(props);
    this.state = {
      writing: false,
      comment: '',
    };
  }
  private textareaRef = createRef<HTMLTextAreaElement>();
  submitComment = (event: any) => {
    if (event.key === 'Enter' && !event.shiftKey) {
      event.preventDefault();
      if (this.state.comment !== '') {
        this.props.addComment(this.state.comment);
      }
      this.setState({ writing: false, comment: '' });
    }
  };
  changeValue = (e: any) => {
    this.setState({ comment: e.target.value });
  };
  writeComment = () => {
    this.setState({ writing: true }, () => {
      const node = this.textareaRef.current;
      node && node.focus();
    });
  };
  public render() {
    return (
      <div className="comments">
        <div className="writing-box">
          <h4>Responses</h4>
          {this.state.writing && (
            <textarea
              ref={this.textareaRef}
              placeholder="Write a response and then press enter... '(shift+enter) for new line'"
              value={this.state.comment}
              onChange={e => this.setState({ comment: e.target.value })}
              cols={30}
              rows={5}
              onKeyPress={event => this.submitComment(event)}
            />
          )}
          {!this.state.writing && (
            <div className="response" onClick={this.writeComment}>
              <CommentIcon />
              <span>Write a response ...</span>
            </div>
          )}
        </div>
        <div className="comments">
          {this.props.comments.map(comment => (
            <Comment key={`comment-${Math.random()}`} {...comment} />
          ))}
        </div>
      </div>
    );
  }
}

export default Comments;
