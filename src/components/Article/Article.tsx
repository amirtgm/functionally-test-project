import React, { Component } from 'react';
// import logo from './logo.svg'
import { IArticle } from '../../types';
import Tag from '../Tag/Tag';
import Clap from '../Clap/Clap';
import Comments from '../Comments/Comments';
import './Article.css';

class Article extends Component<{}, IArticle> {
  constructor(props: {}) {
    super(props);
    this.state = {
      content: `
            Yeah, but I never picked a fight in my entire life. 
            I just wanna use the phone. 
            I'm gonna ram him. I'm writing this down, this is good stuff. Stop it.
      `,
      comments: [],
      clap: 49,
      tags: [{ name: 'Doc' }, { name: 'Marty McFly' }, { name: 'Time Travel' }],
    };
  }
  clapped = () => {
    this.setState({
      clap: this.state.clap + 1,
    });
  };
  addComment = (content: string): void => {
    this.setState({
      comments: [
        {
          name: 'Marty McFly',
          avatar: `avatar-${Math.floor(Math.random() * 9) + 1}`,
          content,
        },
        ...this.state.comments,
      ],
    });
  };
  public render() {
    return (
      <div className="article">
        <p className="serif">{this.state.content}</p>
        <div className="tags">
          {this.state.tags.map(tag => (
            <Tag key={`tag-${tag.name}-${Math.random()}`} name={tag.name} />
          ))}
        </div>
        <Clap claps={this.state.clap} clapped={this.clapped} />
        <div className="divider" />
        <Comments addComment={this.addComment} comments={this.state.comments} />
      </div>
    );
  }
}

export default Article;
