export interface IComment {
  name: string;
  avatar: string;
  content: string;
}
