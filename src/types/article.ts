import { IComment } from './comment'
import { ITag } from './tag'
export interface IArticle {
  content: string
  clap: number
  tags: ITag[]
  comments: IComment[]
}
